// GenCryPWD V3
// Génère et retourne un PWD et un Hash Sha-512bit
// Tout premier programme en C, 1semaine pour finaliser la V1...
// Compiler avec gcc GenCryPWD.c -lcrypt -o GenCryPWD

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <crypt.h>
#include <time.h>

// Caractère au random
char randomOne()
{
    int randResult = random() % (40);
    int randCara;

    char alphaMin[] = "abcdefghijklmnopqrstuvwxyz";
    char alphaMaj[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char specialCara[] = "!#$%&'()*+,-./:;<?@_";
    char result;

    if(randResult < 11)
    {
        randCara = random() % (25);
        result = alphaMin[randCara];
    }
    else if(randResult < 21)
    {
        randCara = random() % (25);
        result = alphaMaj[randCara];
    }
    else if(randResult < 31)
    {
        result = random() % (9) + '0';
    }
    else
    {
        randCara = random() % (19);
        result = specialCara[randCara];
    }

    return result;
}

// Hashage du passwd
char sha5Crypt(char *pwd, char *selKey)
{
    struct crypt_data data;
    data.initialized = 0;

    // Convention pour sha1
    char selType[] = "$6$";
    char selTotal[250], cryptFinal[89], *cryptPOne;

    strcat(selTotal, selType);
    strcat(selTotal, selKey);

    cryptPOne = crypt_r(pwd, selTotal, &data);

    int cryptSize = strlen(cryptPOne);
    // Initialisation des index cryptOne et CryptFinal
    int i = strlen(selKey) + 4, j = 0;

    // Caractères qui indique la fin de la chaine
    cryptFinal[cryptSize - i] = '\0';

    for(; i < cryptSize; ++i)
    {
        cryptFinal[j] = cryptPOne[i];
        ++j;
    }

    printf("%s\n", cryptFinal);
    return 0;
}

// Eviter les types de caractères consecutif
int consecCara(char *testCara, char *pwdCara, int count)
{
    // Retour au dernier candidat de pwd validé
    --count;

    // Compare le dernier char de pwd et le caractère candidat à la table ASCII. Regénère un candidat si ils sont du même "type"
    if(
	((pwdCara[count] >= 65 && pwdCara[count] <= 90) && (testCara[0] >= 65 && testCara[0] <= 90)) ||
	((pwdCara[count] >= 97 && pwdCara[count] <= 122) && (testCara[0] >= 97 && testCara[0] <= 122)) ||
	((pwdCara[count] >= 48 && pwdCara[count] <= 57) && (testCara[0] >= 48 && testCara[0] <= 57)) ||
	(((pwdCara[count] >= 33 && pwdCara[count] <= 47) || (pwdCara[count] >= 58 && pwdCara[count] <= 64) || pwdCara[count] == 95) && ((testCara[0] >= 33 && testCara[0] <= 47) || (testCara[0] >= 58 && testCara[0] <= 64) || testCara[0] == 95))
      )
    {
	return 1;
    }

    return 0;
}

void main(int argc, char *argv[])
{
    // Initialise rand seed
    srandom(time(NULL));

    // Teste si CodeSize est un nombre et qu'il y a bien "2" argv
    if(!isdigit(*argv[1]) || argc != 3)
    {
        printf("Use two argv, no more \nAnd only numbers for CodeSize \nGenCryPWD [CodeSize/0-9] [HashKey/max250]");
	exit(0);
    }

    int totalSize = atoi(argv[1]);
    int count;

    char *pwd, *repeatCara, testCara[2];
    pwd = (char*)calloc(totalSize, sizeof(char));

    // Construction du pwd
    testCara[1] = '\0';
    pwd[totalSize] = '\0';

    for(count = 0; count < totalSize; ++count)
    {
	testCara[0] = randomOne();

	// 82 caractères disponible, si pwd > 82 répétitions inévitable. Limite à 77 pour ne pas mouliner.
	if(totalSize < 77)
	{
	    while((repeatCara = strchr (pwd, testCara[0])) != NULL || consecCara(testCara, pwd, count))
	    {
		testCara[0] = randomOne();
	    }
	}
	else
	{
	    while(consecCara(testCara, pwd, count))
	    {
		testCara[0] = randomOne();
	    }
	}

	pwd[count] = testCara[0];
    }

    // Affichage du passwd et de son hash 512
    printf("%s\n", pwd);
    sha5Crypt(pwd, argv[2]);

    free(pwd);
    exit(0);
}
