// GenPwdInteract
// Génère et retourne un PWD
// Basé sur GenCryPWD V3
// Compiler avec gcc GenPwdInteract.c -o GenPwdInteract

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Caractère au random
char randomOne()
{
    int randResult = random() % (40);
    int randCara;

    char alphaMin[] = "abcdefghijklmnopqrstuvwxyz";
    char alphaMaj[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char specialCara[] = "!#$%&'()*+,-./:;<?@_";
    char result;

    if(randResult < 11)
    {
        randCara = random() % (25);
        result = alphaMin[randCara];
    }
    else if(randResult < 21)
    {
        randCara = random() % (25);
        result = alphaMaj[randCara];
    }
    else if(randResult < 31)
    {
        result = random() % (9) + '0';
    }
    else
    {
        randCara = random() % (19);
        result = specialCara[randCara];
    }

    return result;
}

// Eviter les types de caractères consecutif
int consecCara(char *testCara, char *pwdCara, int count)
{
    // Retour au dernier candidat de pwd validé
    --count;

    // Compare le dernier char de pwd et le caractère candidat à la table ASCII. Regénère un candidat si ils sont du même "type"
    if(
	((pwdCara[count] >= 65 && pwdCara[count] <= 90) && (testCara[0] >= 65 && testCara[0] <= 90)) ||
	((pwdCara[count] >= 97 && pwdCara[count] <= 122) && (testCara[0] >= 97 && testCara[0] <= 122)) ||
	((pwdCara[count] >= 48 && pwdCara[count] <= 57) && (testCara[0] >= 48 && testCara[0] <= 57)) ||
	(((pwdCara[count] >= 33 && pwdCara[count] <= 47) || (pwdCara[count] >= 58 && pwdCara[count] <= 64) || pwdCara[count] == 95) && ((testCara[0] >= 33 && testCara[0] <= 47) || (testCara[0] >= 58 && testCara[0] <= 64) || testCara[0] == 95))
      )
    {
        return 1;
    }

    return 0;
}

void main(int argc, char *argv[])
{
    // Initialise rand seed
    srandom(time(NULL));

    int totalSize, count, again;
    char *pwd, *repeatCara, testCara[2];

    // Recuperation de la longueur de PassWD
    puts("Combien de caractères doit faire votre mot de passe : ");
    scanf("%d", &totalSize);
    // Est-il bien un nombre
    if(totalSize == 0)
    {
        puts("    WARNING : Use only numbers for the CodeSize");
        exit(0);
    }

    pwd = (char*)calloc(totalSize, sizeof(char));

    // Construction du pwd
    do
    {
        testCara[1] = '\0';
        pwd[totalSize] = '\0';

        for(count = 0; count < totalSize; ++count)
        {
            testCara[0] = randomOne();

            // 82 caractères disponible, si pwd > 82 répétitions inévitable. Limite à 77 pour ne pas mouliner.
            if(totalSize < 77)
            {
                while((repeatCara = strchr (pwd, testCara[0])) != NULL || consecCara(testCara, pwd, count))
                {
                    testCara[0] = randomOne();
                }
            }
            else
            {
                while(consecCara(testCara, pwd, count))
                {
                    testCara[0] = randomOne();
                }
            }

            pwd[count] = testCara[0];
        }

        // Affichage du passwd
        printf("%s\n", pwd);

        //
        puts("Regenerer ? 1(yes)/0(No)");
        if(scanf("%d", &again) != 1)
            again = 0;
    }
    while (again == 1);

    puts("Fermeture du programme !");

    free(pwd);
    exit(0);
}
